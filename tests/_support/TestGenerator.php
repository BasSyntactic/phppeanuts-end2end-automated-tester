<?php

use Codeception\Actor;
use Helper\TestWriter;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class TestGenerator extends Actor
{
    use _generated\TestGeneratorActions;

    private function getTestSuite()
    {
        return getenv('APPLICATIONDIR');
    }

    private function getActor()
    {
        return getenv('ACTOR');
    }

    private function getTestSuiteDir()
    {
        return '/tests/' . $this->getTestSuite() . '/';
    }

    public function deletePreviouslyGeneratedTests()
    {
        print(PHP_EOL);
        print('  - Target directory: ' . $this->getTestSuiteDir() . PHP_EOL);
        $files = glob(getcwd() . $this->getTestSuiteDir() . '*' . TestWriter::$filenamePostix . TestWriter::$extension);
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    public function generateTests()
    {
        $I = $this;
        print(PHP_EOL);
        $urls = $this->getUrlsFromMenu($I);
        $numTests = 0;
        foreach ($urls as $url) {
            $testWriter = new TestWriter($url, $this->getTestSuiteDir(), $this->getActor());
            if ($testWriter->writeTest()) {
                $numTests++;
                print('  -> Generated: ' . $testWriter->getGeneratedFilename() . PHP_EOL);
            }
        }
        print(PHP_EOL);
        print('  === Generated ' . $numTests . ' tests! ===' . PHP_EOL);
        print(PHP_EOL);
        print('  Run `php vendor/bin/codecept run ' . $this->getTestSuite() . '` to run these tests... ' . PHP_EOL);
        print(PHP_EOL);
    }

    private function getUrlsFromMenu(TestGenerator $I)
    {
        $urls = $I->grabMultiple('.top-bar-section a', 'href');
        $urls = array_unique($urls);
        return $urls;
    }

    public function dontSeeErrors()
    {
        $I = $this;
        $I->dontSee('E_WARNING');
        $I->dontSee('E_USER_WARNING');
        $I->dontSee('E_NOTICE');
        $I->dontSee('E_USER_NOTICE');
        $I->dontSee('E_ERROR');
        $I->dontSee('E_USER_ERROR');
        $I->dontSee('E_DEPRECATED');
        $I->dontSee('E_STRICT');
    }


}
