<?php

namespace Helper;

use Helper\TemplateHelper;

class TestWriter
{
    /**
     * @var string
     */
    public static $filenamePostix = 'Generated';
    public static $extension = 'Cest.php';
    private $url;
    private $targetDirectory;
    private $actor;
    private $pntType;
    private $filename;
    private $pntHandler;
    private $defaultPntHandler = 'TableSearchPage';


    public function __construct(string $url, string $targetDirectory, string $actor)
    {
        $this->url = $url;
        $this->targetDirectory = $targetDirectory;
        $this->actor = $actor;
    }

    /**
     * @see getMergedValue()
     * @return string
     */
    private function getUrl(){
        return $this->url;
    }

    /**
     * @see getMergedValue()
     * @return string
     */
    private function getActor(){
        return $this->actor;
    }


    public function writeTest()
    {
        if ($this->customTestExists()) return false;
        $filename = $this->getGeneratedFilePath();
        if (is_file($filename)) return false;

        $testClass = $this->getParsedTemplate();

        file_put_contents($filename, $testClass);
        return true;
    }


    private function getParsedTemplate()
    {
        $templateHelper = new TemplateHelper($this->getPntHandler());
        $template = $templateHelper->getTemplate();

        preg_match_all("/<<(.*)>>/iU", $template, $matches);
        $mergeFields = $matches[1];
        foreach($mergeFields as $key => $propName) {
            $template = $this->replaceMergeField($propName, $template);
        }


        return $template;
    }

    private function replaceMergeField($propName,$html) {
        if (strlen($propName)==0) return $html;
        $mergedValue = $this->getMergedValue($propName);
        $html = str_replace('<<'.$propName.'>>',$mergedValue ,$html);
        return $html;
    }

    private function getMergedValue($propName)
    {
        $getter = 'get' . ucFirst($propName);
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }
    }


    private function customTestExists()
    {
        return is_file($this->getFilePath());
    }

    private function getPntType()
    {
        $params = $this->getQueryParams();
        return $this->pntType = isset($params['pntType']) ? $params['pntType'] : '';
    }

    private function getPntHandler()
    {
        $params = $this->getQueryParams();
        return $this->pntHandler = isset($params['pntHandler']) ? $params['pntHandler'] : $this->defaultPntHandler;
    }

    /**
     * @return array
     */
    private function getQueryParams()
    {
        $queryString = parse_url($this->url, PHP_URL_QUERY);
        parse_str($queryString, $queryArray);
        return (array)$queryArray;
    }

    private function getFilePath()
    {
        return getcwd() . $this->targetDirectory . $this->getFilename();
    }

    private function getGeneratedFilePath()
    {
        return getcwd() . $this->targetDirectory . $this->getGeneratedFilename();
    }

    private function getFilename()
    {
        return $this->filename = $this->getPntType() . $this->getPntHandler() . static::$extension;
    }

    public function getGeneratedFilename()
    {
        return $this->filename = $this->getPntType() . $this->getPntHandler() . static::$filenamePostix . static::$extension;
    }


}
