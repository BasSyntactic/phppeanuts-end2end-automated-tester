<?php

namespace Helper;


class TemplateHelper
{
    private $pntHandler;
    private $templateFilename;
    private $templateFilePath;
    private $template;
    private $templatePostfix = 'Cest.template';
    private $defaultTemplateFilePath;
    private $defaultTemplateName = 'Page';
    private $templatesDirectory = '/tests/generateTests/templates/';

    public function __construct(string $pntHandler)
    {
        $this->pntHandler = $pntHandler;
    }

    public function getTemplate()
    {
        $file = $this->getTemplateFilePath();
        if (!file_exists($file)) {
            print('     WARNING: ' . $this->getTemplateFilename() . ' missing, using default "'.$this->getDefaultTemplateFilename().'"...'.PHP_EOL);
            $file = $this->getDefaultTemplateFilePath();
        }
        return $this->template = file_get_contents($file);
    }

    private function getDefaultTemplateFilename()
    {
        return $this->templateFilename = $this->defaultTemplateName . $this->templatePostfix;
    }

    private function getDefaultTemplateFilePath()
    {
        return $this->defaultTemplateFilePath = getcwd() . $this->templatesDirectory . $this->getDefaultTemplateFilename();
    }

    private function getTemplateFilename()
    {
        return $this->templateFilename = $this->pntHandler . $this->templatePostfix;
    }

    private function getTemplateFilePath()
    {
        return $this->templateFilePath = getcwd() . $this->templatesDirectory . $this->getTemplateFilename();
    }


}
