<?php

use \Codeception\Step\Argument\PasswordArgument;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class PntE2eTester extends \Codeception\Actor
{

    protected $env = '.env.example';

    public function __construct($scenario) {
        parent::__construct($scenario);
        $dotenv = Dotenv\Dotenv::createMutable(getcwd(), $this->env);
        $dotenv->load();
    }

    /**
     * Define custom actions here
     */
    public function login()
    {
        $I = $this;
        // if snapshot exists - skipping login
        if ($I->loadSessionSnapshot('login')) {
            return;
        }
        $I->amOnPage('/');
        $I->fillField(getenv('USERNAMEFIELD'), getenv('USERNAME'));
        $I->fillField(getenv('PASSWORDFIELD'), new PasswordArgument(getenv('PASSWORD')));
        $I->click('#butInloggen');
        $I->dontSee('Inloggen vereist');
        // saving snapshot
        $I->saveSessionSnapshot('login');
    }

    public function dontSeeErrors()
    {
        $I = $this;
        $I->dontSee('E_WARNING');
        $I->dontSee('E_USER_WARNING');
        $I->dontSee('E_NOTICE');
        $I->dontSee('E_USER_NOTICE');
        $I->dontSee('E_ERROR');
        $I->dontSee('E_USER_ERROR');
        $I->dontSee('E_DEPRECATED');
        $I->dontSee('E_STRICT');
    }

    /**
     *  klik op de 2e rij buttons van de  EditDetailsPage
     */
    public function allContextButtonsWork()
    {
        $I = $this;
        $contextButtons = $I->grabMultiple('#pntButtonsList div.secondary button:not([disabled])', 'id');
        foreach ($contextButtons as $button) {
            $I->click('#' . $button);
            $I->dontSeeErrors();
            $I->moveBack();
        }
    }

    /**
     *  klik op de 2e rij buttons van de  EditDetailsPage
     */
    public function allTabsWork()
    {
        $I = $this;
        $tabIds = $I->grabMultiple('dl.tabs dd', 'id');
        foreach ($tabIds as $tabId) {
            $I->click('#' . $tabId . ' a');
            $I->dontSeeErrors();
        }
    }

    public function hasPntItemTableItems()
    {
        $I = $this;
        $noItems = $I->grabMultiple('table.pntItemTable tbody td.pntNoItemsTd');
        return (count($noItems) == 0);
    }
}
