<?php
use Codeception\Step\Argument\PasswordArgument;

class BaseTest
{
    protected $env = '.env.example';

    public function loadEnv() {
        $dotenv = Dotenv\Dotenv::createMutable(getcwd(), $this->env);
        $dotenv->load();
    }

    public function deletePreviouslyGeneratedTests(TestGenerator $I)
    {
        $I->deletePreviouslyGeneratedTests();
    }

    public function generateTests(TestGenerator $I)
    {
        $this->login($I);
        $I->generateTests();
    }

    protected function login(TestGenerator $I)
    {
        $I->amOnPage('/' . getenv('APPLICATIONDIR'));
        $I->fillField(getenv('USERNAMEFIELD'), getenv('USERNAME'));
        $I->fillField(getenv('PASSWORDFIELD'), new PasswordArgument(getenv('PASSWORD')));
        $I->click('#butInloggen');
        $I->dontSee('Inloggen vereist');
        $I->dontSeeErrors();
    }


}
