# PhpPeanuts End2End Automated Tester
Geautomatiseerde end to end acceptatie-tests mbv Codeception & ChromeDriver voor PhpPeanuts.
Deze applicatie bevat een test-generator die automatisch tests generereert op basis van een menu, daarnaast kunnen de 
gegenereerde tests mbv ChromeDriver getest worden.

## Inhoudsopgave

- [Installatie](#installatie)
- [Configuratie test generator](#configuratie-test-generator)
- [Configuratie test suite](#configuratie-test-suite)
- [Genereren van tests](#genereren-van-tests)
- [Zelf tests maken](#zelf-tests-maken)
- [Templates van standaard pages](#templates-van-standaard-pages)
- [Todo](#todo)

---

## Installatie

### Stap 1
> Installatie van Codeception en andere modules via composer
```shell
$ composer install 
``` 

### Stap 2
> Kijk welke versie van Chrome je hebt en download de passende 
<a href="https://sites.google.com/a/chromium.org/chromedriver/downloads">ChromeDriver</a>. 
> (meer info: <a href="https://sites.google.com/a/chromium.org/chromedriver/getting-started">hier</a>)

### Stap 3
> Start ChromeDriver en laat deze op de achtergrond draaien.
> Deze stap moet je elke keer doen als je wil gaan testen.
```shell
$ chromedriver --url-base=/wd/hub
```

---

## Configuratie test generator
Na installatie van Codeception & ChromeDriver kunnen we de test-generator configureren, zodat onze er tests voor onze 
applicatie gegenereerd kunnen worden.

In dit specifieke geval gebruik ik `admin` als applicatie om te testen, mocht dat bij jou anders zijn (bv `beheer`) moet 
je dit dus aanpassen.

### Stap 1
> Kopi&euml;ren van `/.env.example` naar `/.env.admin` en de variabelen aanpassen:
```
APPLICATIONDIR=admin
ACTOR=AdminTester
USERNAMEFIELD=gebruikersnaam
PASSWORDFIELD=wachtwoord
USERNAME=eenusername
PASSWORD=eenpassword
```

### Stap 2
We gaan de test-generator opvoeden zodat deze tests kan gaan genereren voor onze test-suite 'admin'.
> Kopieer `/tests/generateTests/exampleCest.php` naar een eigen test `/tests/generateTests/adminCest.php`, en pas  
> `$env` aan naar het juiste bestand:
```php
<?php
require('BaseTest.php');

class adminCest extends BaseTest
{
    protected string $env = '.env.admin';
}

```
De Cest-toevoeging betekent dat het een test is die Codeception kan draaien en is dus verplicht. Zie `BaseTest` voor meer properties/methods die je kunt overschrijven.

---

## Configuratie test suite
Nu gaan we een nieuwe test-suite aanmaken. Een test-suite is een groep van tests.
Per applicatie gebruik één suite (bv 'beheer' of 'admin').

### Stap 1
> Aanmaken test-suite en actor in de shell/cmd:
```shell
$ php vendor/bin/codecept generate:suite admin AdminTester
```
> waarbij `admin` de naam van de test-suite is en `AdminTester` de actor (gebruik dezelfde naam als in stap 1 van de configuratie van test generator).

### Stap 2:
> `/tests/admin.suite.yml` aanpassen, onderstaande toevoegen onder 
`enabled:`
```yaml 
modules:
    enabled:
        - WebDriver:
            url: http://congrezzo.hoe-zo.nl.local:8080/admin
            window_size: false # disabled in ChromeDriver
            port: 9515
            browser: chrome
            capabilities:
                "goog:chromeOptions": # additional chrome options

```
> Vervang de url door je eigen url. 
> Gehele bestand ziet er dan zo uit:
```yaml
actor: AdminTester
modules:
    enabled:
        - WebDriver:
            url: http://congrezzo.hoe-zo.nl.local:8080/admin
            window_size: false # disabled in ChromeDriver
            port: 9515
            browser: chrome
            capabilities:
                "goog:chromeOptions": # additional chrome options
        - \Helper\Admin

```

### Stap 3
> Aanpassen van het bestand `tests/_support/AdminTester.php`:
> 1. afstammen van PntE2eTester.php (ipv \Codeception\Actor)
> 2. `$env` invullen met de juiste bestandsnaam
> 3. eventueel functies overschrijven, zoals public function login() bij aangepaste login
```php
class AdminTester extends PntE2eTester
{
    use _generated\AdminTesterActions;

    /**
     * Define custom actions here
     */
    protected string $env = '.env.admin';
}
```       

---
## Genereren van tests
> Nadat de test-generator en de test-suite goed zijn geconfigureerd, kun je tests laten genereren:
```shell
php vendor/bin/codecept run generateTests adminCest
```
> Waarbij `adminCest` de bestandsnaam zonder .php is in de map `/tests/generateTests/`.
Indien je tests voor alle test-suites wil genereren (stel je hebt meerdere applicaties), dan gebruik je:
```shell
php vendor/bin/codecept run generateTests
```

---
## Het runnen van de (gegenereerde) tests
Nadat de tests zijn gegenereerd voor de test-suite, kun je de test daadwerkelijk runnen.
Let erop dat ChromeDriver draait: ```$ chromedriver --url-base=/wd/hub```
> Draaien van de gehele test-suite       
```shell                           
php vendor/bin/codecept run admin
```
> Het draaien van een test binnen de test-suite       
```shell                           
php vendor/bin/codecept run admin ConEvenementTableSearchPageCest
```
---
## Zelf tests maken
In de map `/tests/admin/` vind je alle (gegenereerde) tests terug.
De gegenereerde tests kun je herkennen aan `GeneratedCest.php`.

Let op: bij het opnieuw genereren van tests, worden alle gegenereerde tests weggegooid en opnieuw aangemaakt 
(voor het geval de templates zijn gewijzigd bijvoorbeeld).

Je kunt een test permanent maken door de `Generated` uit de bestandsnaam (en class) te halen:

```
ConEvenementTableSearchPageGeneratedCest.php => ConEvenementTableSearchPageCest.php
```

Ook staat het je vrij om hier zelf test-bestanden toe te voegen, baseer je daarop op de bestaande tests om te zien 
hoe ze werken.

---
## Templates van standaard pages
In Peanuts zitten natuurlijk veel standaardhandlers, zoals TableSearchPage of EditDetailsPage.
Bij het genereren van tests worden templates geladen voor specifieke handlers als ze bestaan.
Je kunt deze vinden in `/tests/generateTests/templates`.

---
##Todo
* standaard test-database (uitgeklede kopie van live) laden (https://codeception.com/docs/modules/Db)
* meer uitgebreidere tests in de standaard templates
* meer scheiding, dus bij genereren van tests bij TableSearchPages ook EditDetailsPage-tests genereren (of welke handler er dan ook aan hangt).

